$(function() {
    $('.tab_control_link').click(function(e) {
        e.preventDefault();

        var item = $(this).closest('.tab_control_button'),
            contentItem = $('.tabs_item'),
            itemPosition = item.index();

        contentItem.eq(itemPosition)
            .add(item)
            .addClass('active_tab')
            .siblings()
            .removeClass('active_tab');
    })
});