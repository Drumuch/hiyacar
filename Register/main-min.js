function setCookie(e, t, n) {
    var r = new Date;
    r.setDate(r.getDate() + n);
    var i = escape(t) + (n == null ? "" : "; expires=" + r.toUTCString());
    document.cookie = e + "=" + i
}
function getCookie(e) {
    var t, n, r, i = document.cookie.split(";");
    for (t = 0; t < i.length; t++) {
        n = i[t].substr(0, i[t].indexOf("="));
        r = i[t].substr(i[t].indexOf("=") + 1);
        n = n.replace(/^\s+|\s+$/g, "");
        if (n == e)return unescape(r)
    }
}
function handleOccupation(e) {
    $("#frm-occupation, #frm-occupation-fb").keypress(function (e) {
        var t = e.keyCode || e.which;
        if (t != 9) {
            $(this).closest(".occupation").removeClass("has-errors").removeClass("ok");
            hasValidValue = 0
        }
    });
    if (e) {
        $(".ui-autocomplete a").live("click", function () {
            $("#frm-occupation-fb").closest(".occupation").removeClass("has-errors").addClass("ok");
            hasValidValue = 1;
            clicked = true
        })
    } else {
        $(".ui-autocomplete a").live("click", function () {
            $(".occupation").removeClass("has-errors").addClass("ok");
            hasValidValue = 1;
            clicked = true
        })
    }
    $("#frm-occupation, #frm-occupation-fb").on("blur", function () {
        var e = $(this);
        $.ajax({type: "POST", url: "/api/check-occupation", data: {occupation: $(this).val()}}).done(function (t) {
            if (clicked) {
                clicked = false;
                return false
            }
            if (t.exists) {
                e.closest(".occupation").removeClass("has-errors").addClass("ok");
                hasValidValue = 1
            } else {
                e.closest(".occupation").addClass("has-errors").removeClass("ok");
                hasValidValue = 0
            }
        });
        if ($(this).val() != "" && !hasValidValue) {
            $(this).closest(".occupation").addClass("has-errors").removeClass("ok")
        }
    });
    $("#frm-occupation, #frm-occupation-fb").autocomplete({source: "/api/occupation-search", minLength: 2})
}
function facebookBinding(e, t) {
    if (!t) {
        var t = "/"
    }
    $(".fb").on("click", "a", function () {
        FB.login(function (n) {
            if (n.authResponse) {
                FB.api("/me", function (n) {
                    $.ajax({
                        url: e + "/auth/facebook-register",
                        type: "POST",
                        async: false,
                        data: {facebook: n, requestUri: t},
                        success: function (n) {
                            if (n == 1) {
                                window.location = t;
                                return false
                            } else if (n == 2) {
                                window.location = "/register";
                                return false
                            } else {
                                $("#dialog-modal").html(n);
                                $("#dialog-modal").dialog("open");
                                $("#dialog-modal").on("click", ".close", function () {
                                    $("#dialog-modal").dialog("close")
                                });
                                $("#frm-occupation, #frm-occupation-fb").autocomplete({
                                    source: "/api/occupation-search",
                                    minLength: 2
                                });
                                handleOccupation(true);
                                $("#dialog-modal fieldset").on("click", ".terms", function () {
                                    $(this).siblings("ul.errors").slideUp()
                                });
                                $("#dialog-modal fieldset span").on("click", "input", function () {
                                    $(this).siblings("ul.errors").slideUp();
                                    $(this).parents("section").children("ul.errors").slideUp()
                                });
                                $(".facebook-step").on("blur", '.required input[type="text"]', {
                                    linkType: "required",
                                    cssClass: ".required"
                                }, validateInput);
                                $(".facebook-step").on("blur", ".date input", {
                                    linkType: "date",
                                    cssClass: ".date"
                                }, validateInput);
                                $(".facebook-step").on("blur", ".licence input", {
                                    linkType: "licence",
                                    cssClass: ".licence"
                                }, validateInput);
                                $(".facebook-step").on("blur", ".mobile input", {
                                    linkType: "mobile",
                                    cssClass: ".mobile"
                                }, validateInput);
                                $("#dialog-modal").on("click", "input[type='image']:not('.address-lookup,.find-occupation')", function () {
                                    $("form").append('<input type="hidden" name="' + $(this).attr("name") + '" value="1" />');
                                    var t = $(this).closest("form");
                                    $.ajax({
                                        url: e + "/auth/facebook-register",
                                        type: "POST",
                                        data: t.serialize(),
                                        success: function (e) {
                                            if (e == 1) {
                                                window.location = "/account";
                                                return false
                                            } else if (e == 2) {
                                                window.location = "/register";
                                                return false
                                            } else {
                                                $("#dialog-modal").html(e);
                                                $("#frm-occupation, #frm-occupation-fb").autocomplete({
                                                    source: "/api/occupation-search",
                                                    minLength: 2
                                                });
                                                handleOccupation(true);
                                                $("#dialog-modal fieldset").on("click", ".terms", function () {
                                                    $(this).siblings("ul.errors").slideUp()
                                                });
                                                $("#dialog-modal fieldset span").on("click", "input", function () {
                                                    $(this).siblings("ul.errors").slideUp();
                                                    $(this).parents("section").children("ul.errors").slideUp()
                                                });
                                                $("#dialog-modal").on("click", ".close", function () {
                                                    $("#dialog-modal").dialog("close")
                                                });
                                                $(".facebook-step").on("blur", '.required input[type="text"]', {
                                                    linkType: "required",
                                                    cssClass: ".required"
                                                }, validateInput);
                                                $(".facebook-step").on("blur", ".date input", {
                                                    linkType: "date",
                                                    cssClass: ".date"
                                                }, validateInput);
                                                $(".facebook-step").on("blur", ".licence input", {
                                                    linkType: "licence",
                                                    cssClass: ".licence"
                                                }, validateInput);
                                                $(".facebook-step").on("blur", ".mobile input", {
                                                    linkType: "mobile",
                                                    cssClass: ".mobile"
                                                }, validateInput)
                                            }
                                        }
                                    });
                                    return false
                                })
                            }
                        }
                    })
                })
            }
        }, {scope: "email,user_education_history,user_hometown,user_likes,user_location,user_work_history,user_status,user_events,user_birthday"});
        return false
    })
}
function bindAddressLookup() {
    $("#addressLookupResponse").on("click", "a", function () {
        var e = $(this).attr("rel");
        $.ajax({
            url: "/api/full-address/id/" + e, dataType: "json", success: function (e) {
                $('input[name="addressLine1"],input[name="addressLine2"],input[name="city"],input[name="county"]').val("");
                $.each(e.result, function (e, t) {
                    $('input[name="' + e + '"]').val(t).blur();
                    $('input[name="' + e + '"]').closest("span").find("ul.errors").slideUp()
                })
            }
        });
        $(this).closest("ul").slideUp();
        return false
    })
}
function instantiateFacebook(e, t, n, r, i) {
    window.fbAsyncInit = function () {
        function s(e) {
            var t = document.URL;
            if (e.authResponse && 0 == n && !("auth" == r && "register" == i) && !("index" == r && "index" == i)) {
                window.location = "/account"
            }
        }

        function o(e) {
            if (null == e.authResponse && 1 == n) {
                window.location = "/auth/logout"
            }
        }

        FB.init({appId: e, channelUrl: t, status: true, cookie: true, xfbml: true});
        FB.Event.subscribe("auth.login", s);
        FB.Event.subscribe("auth.logout", o)
    };
    (function (e) {
        var t, n = "facebook-jssdk", r = e.getElementsByTagName("script")[0];
        if (e.getElementById(n)) {
            return
        }
        t = e.createElement("script");
        t.id = n;
        t.async = true;
        t.src = "//connect.facebook.net/en_US/all.js";
        r.parentNode.insertBefore(t, r)
    })(document);
    $(".user-info").on("click", ".logout", function () {
        if (FB.getAuthResponse() && 1 == n) {
            FB.logout(function (e) {
                window.location = "/auth/logout"
            })
        } else {
            window.location = "/auth/logout"
        }
        return false
    })
}
function unlinkFacebookBinder(e) {
    $("#facebook-connected").on("click", ".unlink-facebook", function () {
        $.ajax({
            url: e + "/auth/facebook-disconnect", type: "GET", success: function (t) {
                $("#dialog-modal").html(t);
                $("#dialog-modal").on("click", ".close", function () {
                    $("#dialog-modal").dialog("close")
                });
                $("#dialog-modal").dialog("open");
                $("#dialog-modal").on("click", 'input[type="image"]', function () {
                    if ($(this).attr("name") == "action-cancel") {
                        $("#dialog-modal").dialog("close");
                        return false
                    }
                    $("form").append('<input type="hidden" name="' + $(this).attr("name") + '" value="1" />');
                    var t = $(this).closest("form");
                    $.ajax({
                        url: e + "/auth/facebook-disconnect", type: "POST", data: t.serialize(), success: function (e) {
                            if (e) {
                                $("#dialog-modal").html(e);
                                $("#dialog-modal").on("click", ".close", function () {
                                    $("#dialog-modal").dialog("close")
                                })
                            } else {
                                location.reload();
                                return false
                            }
                        }
                    });
                    return false
                })
            }
        });
        return false
    })
}
function doDialog() {
    var e = 960;
    if ($(".login .fb, .unlink-facebook").length)e = 640;
    if ($("#triggerCalendar").length)e = 650;
    if ($(".unlink-facebook").length)e = 470;
    $("#dialog-modal").dialog({
        autoOpen: false, width: e, modal: true, position: "top", close: function () {
            $("#dialog-modal").dialog("destroy").remove();
            $("body").append('<div id="dialog-modal"><div id="loading">Loading</div></div>');
            doDialog()
        }
    })
}
function validateEmail(e) {
    var t = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return t.test(e)
}
function validateDate(e) {
    var t = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    return t.test(e)
}
function validateMobile(e) {
    var t = /^(\s*\d){11}$/;
    return t.test(e)
}
function validateInput(e) {
    if (e.data.linkType == "email") {
        if ($(this).closest(".emailaddress").hasClass("confirmaddress")) {
            var t = $(this).closest(e.data.cssClass).siblings(e.data.cssClass).find("input").val();
            if ($(this).val() != t) {
                $(".confirmaddress").removeClass("ok").addClass("has-errors");
                return false
            }
        }
    }
    if (e.data.linkType == "password") {
        if ($(this).closest(".password").hasClass("confirmpassword")) {
            var t = $(this).closest(e.data.cssClass).siblings(e.data.cssClass).find("input").val();
            if ($(this).val() != t) {
                $(".confirmpassword").removeClass("ok").addClass("has-errors");
                return false
            }
        }
    }
    if (!$(this).val()) {
        $(this).closest(e.data.cssClass).removeClass("ok").addClass("has-errors");
        return
    }
    if (e.data.linkType == "mobile" && !validateMobile($(this).val())) {
        $(this).closest(e.data.cssClass).removeClass("ok").addClass("has-errors");
        return
    }
    if (e.data.linkType == "email" && !validateEmail($(this).val())) {
        $(this).closest(e.data.cssClass).removeClass("ok").addClass("has-errors");
        return
    }
    if (e.data.linkType == "date" && !validateDate($(this).val())) {
        $(this).closest(e.data.cssClass).removeClass("ok").addClass("has-errors");
        return
    }
    var n = new Date;
    if (e.data.linkType == "dob") {
        var r = $("." + e.data.cssClass + ' input[placeholder="dd"]').val();
        var i = $("." + e.data.cssClass + ' input[placeholder="mm"]').val() - 1;
        var s = $("." + e.data.cssClass + ' input[placeholder="yyyy"]').val();
        var o = r + "/" + (i + 1) + "/" + s;
        var u = new Date(s, i, r);
        var a = new Date(u.getFullYear() + 18, u.getMonth(), u.getDate());
        if (!validateDate(o) || a >= n) {
            $(this).closest(e.data.cssClass).removeClass("ok").addClass("has-errors");
            return
        }
    }
    if (e.data.linkType == "licence") {
        var r = $("." + e.data.cssClass + ' input[placeholder="dd"]').val();
        var i = $("." + e.data.cssClass + ' input[placeholder="mm"]').val() - 1;
        var s = $("." + e.data.cssClass + ' input[placeholder="yyyy"]').val();
        var f = $('.dob input[placeholder="dd"]').val();
        var l = $('.dob input[placeholder="mm"]').val() - 1;
        var c = $('.dob input[placeholder="yyyy"]').val();
        var o = r + "/" + (i + 1) + "/" + s;
        var u = new Date(c, l, f);
        var h = new Date(u.getFullYear() + 17, u.getMonth(), u.getDate());
        var p = new Date(s, i, r);
        if (!validateDate(o) || h >= p) {
            $(this).closest(e.data.cssClass).removeClass("ok").addClass("has-errors");
            return
        }
    }
    var d = /\d/g;
    if (e.data.linkType == "password" && $(this).val().length < 6 || e.data.linkType == "password" && d.test($(this).val()) == false) {
        $(this).closest(e.data.cssClass).removeClass("ok").addClass("has-errors");
        return
    }
    $(this).closest(e.data.cssClass).removeClass("has-errors").addClass("ok");
    $(this).siblings("ul.errors").remove()
}
function bindBookingMaps() {
    var e = $(".map").not(".activated").attr("data-id");
    var t = new google.maps.LatLng($("#map" + e).attr("data-lat"), $("#map" + e).attr("data-lng"));
    var n = {zoom: 12, center: t, mapTypeId: google.maps.MapTypeId.ROADMAP, scrollwheel: false};
    var r = new google.maps.Map(document.getElementById("map" + e), n);
    var i = new google.maps.Marker({position: t, map: r});
    i.setIcon("/static/images/bg-map-icon-a.png", [32, 41]);
    $("#map" + e).addClass("activated")
}
function clickOnce() {
    if (clickFlag == false) {
        clickFlag = true;
        $(".clickOnce").after('<img src="/static/images/btn-confirm-lg-loading.png" alt="Loading..." class="loading-btn" />');
        $(".clickOnce").hide();
        clickTimer = setTimeout("resetClickFlag()", 5e3)
    } else {
        return false
    }
}
function resetClickFlag() {
    clearTimeout(clickTimer);
    $(".loading-btn").remove();
    $(".clickOnce").show();
    clickFlag = false
}
var letters = new Array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j");
var WEB_ROOT = (document.location.protocol == "https:" ? "https://" : "http://") + document.location.host;
var hasValidValue = 0;
var clicked = false;
$(function () {
    if ($("#frm-too-new-for-service").length && $("#frm-too-new-for-service").is(":checked")) {
        $(".dates").hide()
    }
    $(".further-detail").on("click", "#frm-too-new-for-service", function () {
        if ($("#frm-too-new-for-service").is(":checked")) {
            $(".dates select option").removeAttr("selected");
            $(".dates .select").each(function () {
                var e = $(this).find('option[value=""]').attr("label");
                $(this).find(".list-heading em").text(e)
            });
            $(".dates").slideUp()
        } else {
            $(".dates").slideDown()
        }
    });
    var e = navigator.userAgent.toLowerCase();
    var t = {isIE: e.indexOf("msie") > -1, isIE6: e.indexOf("msie 6") > -1};
    $("body").prepend('<div id="sk-notices" style="z-index:9999;"></div>');
    if (t.isIE && t.isIE6) {
        var n = "http://www.microsoft.com/windows/downloads/ie/getitnow.mspx";
        var r = "http://www.mozilla.com/firefox/";
        var i = "http://www.google.com/chrome/";
        $("#sk-notices").append('<div style="background-color:#F9B8B8;border-bottom:2px solid #DEA4A4;font-family:Verdana,sans-serif;font-size:12px;text-align:center;color:#333;"><p style="padding:6px 15px;margin:0;">You are using an out-of-date and unsupported version of Internet Explorer, <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="' + n + '">click here</a> to download the latest version. Fancy a better browser? Try <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="' + r + '">Mozilla Firefox</a> or <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="' + i + '">Google Chrome</a></p></div>')
    }
    if (getCookie("username") == null || getCookie("username") == "") {
        var s = WEB_ROOT + "/privacy-policy";
        var o = "http://civicuk.com/cookie-law/browser-settings";
        $("#sk-notices").append('<div style="background-color:#E8FDCE;border-bottom:2px solid #8BC541;font-family:Verdana,sans-serif;font-size:12px;text-align:center;color:#333;position:relative;"><p style="padding:6px 50px 6px 10px;margin:0;">This website uses cookies to store your data, for more information see our <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" href="' + s + '">cookie policy</a> and <a style="text-decoration:underline;color:#0D394D;font-weight:bold;" target="_blank" href="' + o + '">change your settings</a> at any time <a href="#" id="sk-cc-close" style="text-decoration:none;color:#0D394D;font-weight:bold;position:absolute;top:6px;right:10px;">Close</a></p></div>')
    }
    $("#sk-notices").on("click", "#sk-cc-close", function () {
        setCookie("username", true, 365);
        $(this).parents("div").eq(0).slideUp();
        return false
    });
    doDialog();
    $(".jsOnly").css("display", "block");
    $("body").on("click", ".inactive", false);
    $(".hideOnLoad, .availability-search_vehicle .availability, .how-panel section:not(:first-child), .how-panel article div").hide();
    $(".hideOnLoad .errors").each(function () {
        var e = $(this).parents(".hideOnLoad");
        $('a[data-rel="' + e.attr("id") + '"]').hide();
        e.show()
    });
    if ($.browser.msie) {
        $("#my-car #tips, .primary li a, #add-vehicle .price-matrix, .secondary li a, header .auth, .home form, .home #main, .home .banner, .layout-b .panel-a, .layout-f .panel-b, .how-panel, .how-panel article, .availability-selector, .layout-f .panel-a, .layout-g .panel-a, .masthead, .payment-history, .payment-history article, .layout-f .panel-a .user-ratings, .message-listing, .bookings-grid, #dashboard #rent-your-car, #support-message, #dashboard #rent-a-car, #dashboard #my-car").addClass("corners").append('<div class="corner tl" /><div class="corner tr" /><div class="corner bl" /><div class="corner br" />');
        $("#main").addClass("corners").append('<div class="corner blm" /><div class="corner brm" />')
    }
    $(".data-input fieldset span, .date-field").on("click", "input", function () {
        $(this).siblings("ul.errors").slideUp();
        $(this).parents("section").children("ul.errors").slideUp()
    });
    $(".data-input fieldset").on("click", ".terms", function () {
        $(this).siblings("ul.errors").slideUp()
    });
    $(".availability-search_vehicle .images img").each(function () {
        $(this).parent("a").attr("href", $(this).attr("data-full-url"))
    });
    $(".availability-search_vehicle .images a").lightBox({
        imageLoading: "/static/images/bg-ajax-loader.gif",
        imageBtnClose: "/static/images/btn-close.gif"
    });
    $("#frm-password, #frm-new-password").closest("div").after('<div id="password-strength"><p id="password-strength-result" class="w3c"></p><em id="graybar"></em><em id="colourbar"></em><p id="password-strength-label">Password strength</p></div>');
    var u = 0;
    $("#frm-password, #frm-new-password").keyup(function () {
        $("#password-strength-result").html(passwordStrength($(this).val(), ""));
        u = passwordStrengthPercent($(this).val(), "");
        $("#colourbar").css({width: u * 1.3 + "px"})
    });
    if ($("#frm-password").length) {
        $("#password-strength-result").html(passwordStrength($("#frm-password").val(), ""));
        u = passwordStrengthPercent($("#frm-password").val(), "");
        $("#colourbar").css({width: u * 1.3 + "px"})
    }
    $("form.user-registration").on("blur", '.required input[type="text"]', {
        linkType: "required",
        cssClass: ".required"
    }, validateInput);
    $("form.user-registration").on("blur", ".emailaddress input", {
        linkType: "email",
        cssClass: ".emailaddress"
    }, validateInput);
    $("form.user-registration").on("blur", ".date input", {linkType: "date", cssClass: ".date"}, validateInput);
    $("form.user-registration").on("blur", ".dob input", {linkType: "dob", cssClass: ".dob"}, validateInput);
    $("form.user-registration").on("blur", ".licence input", {
        linkType: "licence",
        cssClass: ".licence"
    }, validateInput);
    $("form.user-registration").on("blur", ".mobile input", {linkType: "mobile", cssClass: ".mobile"}, validateInput);
    $("form.user-registration").on("blur", ".password input", {
        linkType: "password",
        cssClass: ".password"
    }, validateInput);
    $("body").on("click", ".list-heading", function () {
        $(this).siblings("ul").show()
    });
    if ($(".stage1").length) {
        $("input").on("change", function () {
            if ($('input[type="text"]').val() != "" && $('input[type="checkbox"]').attr("checked") == "checked") {
                $(".stage1 .continue-step-2").removeClass("inactive").fadeTo(1, 1)
            } else {
                $(".stage1 .continue-step-2").addClass("inactive").fadeTo(1, .5)
            }
        });
        if (!$('input[type="checkbox"]:not(:checked)').length && !$('input[type="text"][value=""]').length) {
            $(".stage1 .continue-step-2").removeClass("inactive").fadeTo(1, 1)
        } else {
            $(".stage1 .continue-step-2").addClass("inactive").fadeTo(1, .5)
        }
    }
    if ($(".stage2").length) {
        $("input").on("change", function () {
            if (!$('input[type="checkbox"]:not(:checked)').length) {
                $(".stage2 .continue-step-3").removeClass("inactive").fadeTo(1, 1)
            } else {
                $(".stage2 .continue-step-3").addClass("inactive").fadeTo(1, .5)
            }
        });
        if (!$('input[type="checkbox"]:not(:checked)').length) {
            $(".stage2 .continue-step-3").removeClass("inactive").fadeTo(1, 1)
        } else {
            $(".stage2 .continue-step-3").addClass("inactive").fadeTo(1, .5)
        }
        if ($(".claims-history .claim").length || $("h3.failed").length) {
            $(".stage2 .continue-step-3").addClass("inactive").fadeTo(1, .5)
        }
    }
    $(".licence label").on("click", "em.help", function () {
        $("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Licence Issue Date...</h1><h2>How to find it on your drivin dsg licence.</h2><p><img src="/static/images/img-licence-dates.jpg" alt="" /></p></div>');
        $("#dialog-modal").dialog({
            autoOpen: false,
            modal: true,
            resizable: true,
            draggable: true,
            closeOnEscape: true,
            position: ['center',20],
            title: "Trazoo"}).dialog("open");
        $(".helper").on("click", ".close", function () {
            $("#dialog-modal").dialog("close")
        })
    });
    $(".licence-num label").on("click", "em.help", function () {
        $("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Licence Number...</h1><h2>How to find it on your drivindsdssd g licence.</h2><p><img src="/static/images/img-licence-number.jpg" alt="" /></p></div>');
        $("#dialog-modal").dialog({
            autoOpen: false,
            modal: true,
            resizable: true,
            draggable: true,
            closeOnEscape: true,
            position: ['center',20],
            title: "Trazoo"}).dialog("open");
        $(".helper").on("click", ".close", function () {
            $("#dialog-modal").dialog("close")
        })
    });
    $(".date label").on("click", "em.help", function () {
        $("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Licence Issue Date...</h1><h2>How to find it on your driving licence.</h2><p><img src="/static/images/img-licence-dates.jpg" alt="" /></p></div>');
        $("#dialog-modal").dialog({
            autoOpen: false,
            modal: true,
            resizable: true,
            draggable: true,
            closeOnEscape: true,
            position: ['center',20],
            title: "Trazoo"}).dialog("open");
        $(".helper").on("click", ".close", function () {
            $("#dialog-modal").dialog("close")
        })
    });
    $(".mobile label").on("click", "em.help", function () {
        $("#dialog-modal").html('<div class="helper"><em class="close">Close</em><h1>Mobile Phone Number...</h1><h2>We need your mobile number so that we can contact you with specific information about your car rentals with HiyaCar. We don\'t use this for advertising and never pass it on to third parties.</h2></div>');
        $("#dialog-modal").dialog({
            autoOpen: false,
            modal: true,
            resizable: true,
            draggable: true,
            closeOnEscape: true,
            position: ['center',20],
            title: "Trazoo"}).dialog("open");
        $(".helper").on("click", ".close", function () {
            $("#dialog-modal").dialog("close")
        })
    });
    if ($("#warningPanel").length) {
        $("#dialog-modal").html('<div id="warningPanel"><em class="close">Close</em>' + $("#warningPanel").html() + "</div>");
        $("#dialog-modal").dialog({
            autoOpen: false,
            modal: true,
            resizable: true,
            draggable: true,
            closeOnEscape: true,
            position: ['center',20],
            title: "Trazoo"}).dialog("open");
        $(".layout-a > #warningPanel").remove();
        $("#dialog-modal").on("click", ".close, .overlayclose", function () {
            $("#dialog-modal").dialog("close")
        })
    }
    $(window).on("mouseleave", ".select", function () {
        $(this).find("ul").hide()
    });
    if ($(".reviews li").length <= 1) {
        $(".review-actions").hide()
    }
    $(".review-actions").on("click", ".next", function () {
        var e = $(".reviews li.active");
        var t = e.next("li");
        var n = $(".reviews li:first-child");
        e.hide().removeClass("active");
        if (t.length) {
            t.show().addClass("active")
        } else {
            n.show().addClass("active")
        }
        return false
    });
    $(".review-actions").on("click", ".prev", function () {
        var e = $(".reviews li.active");
        var t = e.prev("li");
        var n = $(".reviews li:last-child");
        e.hide().removeClass("active");
        if (t.length) {
            t.show().addClass("active")
        } else {
            n.show().addClass("active")
        }
        return false
    });
    $(".select a").live("click", function (e) {
        $(this).parents("ul").find("a").removeClass("selected");
        $(this).parents("ul").siblings(".list-heading").children("em").text($(this).text());
        $(this).addClass("selected");
        $(this).parents("ul").hide();
        $(this).parents("div").siblings("select").children("option").removeAttr("selected");
        $(this).parents("div").siblings("select").children('option[value="' + $(this).attr("rel") + '"]').attr("selected", "selected");
        return false
    });
    $(window).on("click", 'input[name="action[addressLookup]"]', function () {
        var e = $("#frm-postcode").val();
        if (!e) {
            var e = $("#frm-postcode-fb").val()
        }
        if (!e)return false;
        var t = $(this).parent("span").parent("div");
        var n = '<div id="addressLookupResponse"><label class="select-label">Select an address, or enter the address below:</label><div class="select lg"><select id="frm-address" name="address" style="display: none;"><option label="Select an address" value="">Select an address</option>';
        var r = "</select>";
        var i = "";
        var s = '<div><p class="list-heading"><em>Select an address</em></p><ul data-rel="frm-address" style="display: none;"><li><a rel="" href="#Select an address">Select an address</a></li>';
        var o = "</ul></div></div></div>";
        var u = "";
        $.ajax({
            url: "/api/postcode-addresses/postcode/" + e, dataType: "json", success: function (e) {
                $.each(e.result, function (e, t) {
                    i += '<option value="' + t["id"] + '">' + t["streetAddress"] + "</option>";
                    u += '<li><a rel="' + t["id"] + '" href="#' + t["streetAddress"] + '">' + t["streetAddress"] + "</a></li>"
                });
                if (!$("#addressLookupResponse").length) {
                    t.after(n + i + r + s + u + o);
                    $("#hiddenAddressFields").show()
                } else {
                    $("#addressLookupResponse").replaceWith(n + i + r + s + u + o);
                    $("#hiddenAddressFields").show()
                }
                bindAddressLookup()
            }
        });
        return false
    });
    $(".user-ratings").css("overflow", "hidden");
    $("#hownav li:first-child").addClass("selected");
    $("#hownav").on("click", "a", function () {
        var e = $(this).attr("data-rel");
        $("#hownav li").removeClass("selected");
        $(this).parent("li").addClass("selected");
        $(".how-panel section").hide();
        $('.how-panel section[data-rel="' + e + '"]').show();
        return false
    });
    $(".how-panel article").addClass("expandable contracted");
    $(".how-panel").on("click", "article h3 em", function () {
        $(this).parent("h3").siblings("div").slideToggle();
        $(this).parent("h3").parent("article").toggleClass("contracted")
    });
    $("#when-datepicker").datepicker();
    if ($("#frm-search-start-at").length)handleDateRangePickers($("#frm-search-start-at"), $("#frm-search-end-at"), false);
    if ($("#frm-vehicle-booking-start-at").length)handleDateRangePickers($("#frm-vehicle-booking-start-at"), $("#frm-vehicle-booking-end-at"), $("#bookingVehicleId").attr("data-rel"));
    if ($("#frm-booking-start-at").length)handleDateRangePickers($("#frm-booking-start-at"), $("#frm-booking-end-at"), $("#bookingVehicleId").attr("data-rel"));
    if ($("#frm-start-at").length)handleDateRangePickers($("#frm-start-at"), $("#frm-end-at"), false);
    $("#frm-last-serviced-at").datepicker({dateFormat: "dd/mm/yy"});
    $(".date").on("click", function () {
        $(this).siblings('input[type="text"]').click();
        return false
    });
    var a = 0;
    var f = 200;
    $(".home #main .feature").each(function () {
        var e = $(this).css("top");
        $(this).css("top", "-300px");
        $(this).delay(a * f).animate({top: e}, 1e3, "easeOutBounce");
        a++
    });
    var l;
    $("#dialog-modal").on("click", ".action input:not(.confirm)", function () {
        $("#dialog-modal").dialog("close");
        return false
    });
    if ($("body").hasClass("availability-search_vehicle")) {
        var c = location.pathname.match(/\/id\/(\d+)/);
        if (c) {
            $.ajax("/api/visit-vehicle/id/" + c[1])
        }
    }
    if ($("body").hasClass("availability-search_index") && $("#map").length || $("body").hasClass("availability-search_view-shortlist") && $("#map").length) {
        var h = new google.maps.Map(document.getElementById("map"), {
            zoom: 10,
            center: new google.maps.LatLng(-33.92, 151.25),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        });
        var p = new google.maps.LatLngBounds;
        var d, v;
        var a = 0;
        $(".vehicle").each(function () {
            var e = $(this);
            var t = $(this).attr("data-lat");
            var n = $(this).attr("data-lng");
            var r = $(this).find(".view-details").attr("href");
            var i = new google.maps.LatLng(t, n);
            p.extend(i);
            d = new google.maps.Marker({position: i, map: h});
            if (a <= 9) {
                d.setIcon("/static/images/bg-map-icon-" + letters[a] + ".png", [32, 41])
            } else {
                d.setIcon("/static/images/bg-map-icon-more.png", [32, 41])
            }
            h.setCenter(p.getCenter());
            h.fitBounds(p);
            google.maps.event.addListener(d, "click", function (e, t) {
                return function () {
                    window.location = r
                }
            }(d, v));
            google.maps.event.addListener(d, "mouseover", function (t, n) {
                return function () {
                    e.addClass("highlight")
                }
            }(d, v));
            google.maps.event.addListener(d, "mouseout", function (t, n) {
                return function () {
                    e.removeClass("highlight")
                }
            }(d, v));
            a++
        });
        $("#map").stickySidebar({constrain: true})
    }
    if ($("body").hasClass("availability-search_vehicle")) {
        var m = new google.maps.LatLng($("#map").attr("data-lat"), $("#map").attr("data-lng"));
        var g = {zoom: 12, center: m, mapTypeId: google.maps.MapTypeId.ROADMAP, scrollwheel: false};
        var h = new google.maps.Map(document.getElementById("map"), g);
        var d = new google.maps.Marker({position: m, map: h});
        d.setIcon("/static/images/bg-map-icon-a.png", [32, 41])
    }
    if ($("body").hasClass("account-rentals_index") && $(".map").length) {
        bindBookingMaps()
    }
    if ($("#resultCount").length) {
        $("#resultCount").after('<form id="filter"><label id="filterLabel">Ordered by:</label></form><form id="filter2"><label id="filterLabel2">Distance:</label></form>');
        $("#sortFilter").clone().appendTo("#filter");
        $("#distanceFilter").clone().appendTo("#filter2");
        $("#filter").on("click", "a", function () {
            $('#sortFilter option[value="' + $(this).attr("rel") + '"]').attr("selected", "selected");
            $("form.search").submit();
            return false
        });
        $("#filter2").on("click", "a", function () {
            $('#distanceFilter option[value="' + $(this).attr("rel") + '"]').attr("selected", "selected");
            $("form.search").submit();
            return false
        })
    }
    $(".search").on("click", 'ul[data-rel="frm-search-make-id"] a', function () {
        var e = $(this).attr("rel");
        var t = $("#frm-search-model-id");
        var n = $('ul[data-rel="frm-search-model-id"]');
        var r = $('ul[data-rel="frm-search-model-id"]').siblings(".list-heading").children("em");
        if (e) {
            $.getJSON("/api/models/makeId/" + e, function (e) {
                t.html("");
                n.html("");
                r.text("All models");
                $.each(e.result, function (e, r) {
                    t.append($("<option></option>").attr("value", e).text(r));
                    n.append($('<li><a rel="' + e + '" href="' + r + '">' + r + "</a></li>"))
                })
            })
        }
    });
    $(".limit input[maxlength], .limit textarea[maxlength]").each(function () {
        var e = $(this).attr("maxlength");
        $(this).parent("span").after('<p class="charsRemaining">0 / ' + e + " characters remaining</p>");
        if ($(this).val().length > e) {
            $(this).val($(this).val().substr(0, e))
        }
        $(this).parent("span").siblings(".charsRemaining").text(e - $(this).val().length + " / " + e + " characters remaining");
        $(this).keyup(function () {
            if ($(this).val().length > e) {
                $(this).val($(this).val().substr(0, e))
            }
            $(this).parent("span").siblings(".charsRemaining").text(e - $(this).val().length + " / " + e + " characters remaining")
        })
    });
    if ($(".accident-claims").length > 0) {
        $.ajax({
            url: "/service/claims", success: function (e) {
                e = jQuery.parseJSON(e);
                $(".accident-claims").removeClass("loading");
                $(".accident-claims .accidents").text(e.accidents);
                $(".accident-claims .thefts").text(e.thefts);
                if (e.can_hire) {
                    $(".accident-claims").addClass("passed")
                } else {
                    $(".accident-claims").addClass("failed");
                    $(".user-confirm").hide();
                    $('input[type="image"]:not(.previous)').hide()
                }
            }
        })
    }
    $(".select select").each(function () {
        var e = $(this).attr("id");
        var t = $(this).parents(".select");
        var n = "";
        var r = "";
        $("option", this).each(function () {
            if ($(this).attr("selected") || !$(this).val()) {
                r = $(this).attr("label")
            }
            n += '<li><a href="#' + $(this).attr("label") + '" rel="' + $(this).val() + '">' + $(this).attr("label") + "</a></li>"
        });
        var i = '<div><p class="list-heading"><em>' + r + '</em></p><ul data-rel="' + e + '">' + n + "</ul></div>";
        t.append(i);
        $(this).hide()
    });
    handleOccupation(false)
});
var clickFlag = false;
var clickTimer;